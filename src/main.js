// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import firebase from 'firebase'
import VueFirebase from 'vue-firebase'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify, {
  theme: {
    primary: '#0D47A1',
    secondary: '#1976D2',
    accent: '#EF6C00',
    error: '#f44336',
    warning: '#ffeb3b',
    info: '#2196f3',
    success: '#4caf50'
  }
})
Vue.config.productionTip = false

let app
// Initialize Firebase
let config = {
  apiKey: 'AIzaSyB2pJlP5WF7eV-Bb0xwUhQVQAp6JPPt1h4',
  authDomain: 'ecci-coin.firebaseapp.com',
  databaseURL: 'https://ecci-coin.firebaseio.com',
  projectId: 'ecci-coin',
  storageBucket: '',
  messagingSenderId: '671764595978'
}

Vue.use(VueFirebase, { firebase: firebase, config: config })

firebase.auth().onAuthStateChanged(function (user) {
  if (!app) {
    if (user) {
      store.commit('setUser', user)
      store.commit('setMensajeSnackBar',
        { snackbar: true,
          color: '',
          mode: '',
          timeout: 6000,
          text: 'Conectado...'
        })
      router.replace('bloques')
    }
    /* eslint-disable no-new */
    app = new Vue({
      el: '#app',
      router,
      store,
      components: { App },
      template: '<App/>'
    })
  }
})
