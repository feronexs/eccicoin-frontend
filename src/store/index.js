import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // direción de minado
    api: 'http://ec2-54-233-142-88.sa-east-1.compute.amazonaws.com:3000',
    balance: 0,
    bloques: [],
    usuario: null,
    perfil: null,
    loading: false,
    error: null,
    mensaje: null,
    snackbarGlobal: {
      snackbar: false,
      color: '',
      mode: '',
      timeout: 6000,
      text: ''
    }
  },
  mutations: {
    setUser (state, payload) {
      state.usuario = payload
    },
    setMensajeSnackBar (state, payload) {
      state.snackbarGlobal = payload
    }
  },
  actions: {
    setUser ({ commit }, payload) {
      commit('setUser', payload)
    },
    setMensaje ({ commit }, payload) {
      commit('set')
    },
    setMensajeSnackBar ({ commit }, payload) {
      commit('setMensajeSnackBar', payload)
    }
  },
  getters: {
    usuario (state) {
      return state.usuario
    },
    error (state) {
      return state.error
    },
    mensaje (state) {
      return state.mensaje
    },
    balance (state) {
      return state.balance
    }
  }
})
