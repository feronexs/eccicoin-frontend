import Vue from 'vue'
import Router from 'vue-router'
import Bloques from '@/components/Bloques'
import Login from '@/components/Login'
import MiBilletera from '@/components/MiBilletera'
import MiPerfil from '@/components/MiPerfil'
import firebase from 'firebase'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '*',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/bloques',
      name: 'Bloques',
      component: Bloques,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/perfil',
      name: 'MiPerfil',
      component: MiPerfil,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/billetera',
      name: 'MiBilletera',
      component: MiBilletera,
      meta: {
        requiresAuth: true
      }
    }
  ],
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  if (requiresAuth && !currentUser) next('login')
  else if (!requiresAuth && currentUser) next()
  else next()
})

export default router
