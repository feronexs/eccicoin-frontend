# Proyecto ECCICOIN WEB

Este es un prototipo creado en Node js para la criptomoneda de la UECCI

### Prerequisitos

Instalar Nodejs

https://nodejs.org/es/

Instalar git

https://git-scm.com


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## bitbucket

https://bitbucket.org/feronexs/eccicoin-frontend

## Autores

* Erik Araujo - erikf.araujof@ecci.edu.co
* Jaime Camargo - jaime.camargoa@ecci.edu.co
* Jullie Penagos - julliepenagosa@ecci.edu.co

## License

MIT License
